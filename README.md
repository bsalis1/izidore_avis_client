## Installation et lancement:

- Cloner le repository
- Se positionner sur la branch "master"
- configurer votre SGBDR dans le fichier ".env" qui se trouve dans le dossier app.

- Dans le dossier "app", ouvrir un terminal et executer les commandes:
1. "composer install"
2. "symfony console doctrine:database:create"
3. "symfony console doctrine:migration:migrate"
4. "symfony console doctrine:fixtures:load"
5. "npm i"
6. "npm run watch"
7. "symfony serve"

## Routes:

- Inscription: "http://localhost:8000/inscription"
- Connexion: "http://localhost:8000/connexion"
- Deconnexion: "http://localhost:8000/deconnexion"
- Page d'Emma: "http://localhost:8000/member"

