import $ from 'jquery';

export const review = () => {

    function loadReviews() {
        $.ajax({
            type: 'GET',
            url: "http://localhost:8000/review/get?seller=emma",
            error: function (e) {
                console.log(e);
            }
        }).then(function (response) {
            if (response.reviews.length) {
                reviewsCarousel(response.reviews);
            };
        })
    }
    loadReviews();

    let i = 0;
    let count = 3;

    function reviewsCarousel(reviews) {
        $("#no-comment").text("À propos d'Emma");
        for (i; i < count; i++) {
            if (reviews[i]) {
                let card = `
                <div class="card me-3 w-100 mb-4 mt-4" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="fw-bold mb-2">${reviews[i].user.username} :</h5>
                        <p class="card-text">"${reviews[i].content}"</p>
                    </div>
                </div>
                `
                $("#reviews-container").append(card)
            }
        }
    }

    $("#btn-review-form").click(function (event) {
        event.preventDefault();

        let test = $("#review-content").val();

        $.ajax({
            type: 'POST',
            url: "http://localhost:8000/review/add",
            data: {
                content: test
            },
            error: function (e) {
                console.log(e);
            }
        }).then(function (response) {
            addReview(response.author, response.review);
        })
    })

    function addReview(author, review) {
        $("#no-comment").text("À propos d'Emma");
        let card = `
        <div class="card me-3 w-100 mb-4 mt-4" style="width: 18rem;">
            <div class="card-body">
                <h5 class="fw-bold mb-2">${author} :</h5>
                <p class="card-text">"${review}"</p>
            </div>
        </div>
        `
        $("#reviews-container").append(card)
    }


}

export default review;