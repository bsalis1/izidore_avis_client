import $ from 'jquery';

export const header = () => {
    $(window).scroll(function() {
        if (window.scrollY !== 0) {
            $("#header-bot, #header-bot *").slideUp("fast");
        } else {
            $("#header-bot, #header-bot *").slideDown("fast");
        }
    })

}

export default header;