// any CSS you import will output into a single css file (app.css in this case)
import './styles/global.scss';

// start the Stimulus application
import './bootstrap';

// loads the jquery package from node_modules
import $ from 'jquery';

// import the function from greet.js (the .js extension is optional)
// ./ (or ../) means to look for a local file
import header from './components/header.js';
import memberCard from './components/memberCard';
import review from './components/review';

$(function() {
    header();
    memberCard();
    review();
});
