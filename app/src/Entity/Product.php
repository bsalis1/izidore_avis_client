<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $old_price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Supplier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliverer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOldPrice(): ?int
    {
        return $this->old_price;
    }

    public function setOldPrice(int $old_price): self
    {
        $this->old_price = $old_price;

        return $this;
    }

    public function getSupplier(): ?string
    {
        return $this->Supplier;
    }

    public function setSupplier(string $Supplier): self
    {
        $this->Supplier = $Supplier;

        return $this;
    }

    public function getDeliverer(): ?string
    {
        return $this->deliverer;
    }

    public function setDeliverer(?string $deliverer): self
    {
        $this->deliverer = $deliverer;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
