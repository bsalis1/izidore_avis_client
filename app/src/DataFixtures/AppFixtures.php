<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 12; $i++) {
            $product = new Product();
            $product->setTag('product ' . $i);
            $product->setPrice(mt_rand(10, 30));
            $product->setOldPrice(mt_rand(35, 60));
            $product->setSupplier('Maisons du Monde');
            $product->setDeliverer($this->delivererOrnot());
            $product->setImage('plante');
            $manager->persist($product);
        }

        $manager->flush();
    }

    public function delivererOrNot() {
        $trueOrFalse = mt_rand(0, 1);
        return $trueOrFalse != 0 ? "Mondial Relay" : null;
    }
}
