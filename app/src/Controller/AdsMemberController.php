<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdsMemberController extends AbstractController
{

    /**
     * @Route("/membre", name="ads_member")
     */
    public function index(): Response
    {

        $products = $this->getDoctrine()
        ->getRepository(Product::class)
        ->findAll();

        return $this->render('ads_member/index.html.twig', [
            "products" => $products
        ]);
    }
}
