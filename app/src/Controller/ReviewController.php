<?php

namespace App\Controller;

use App\Entity\Review;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends AbstractController
{
    /**
    * @Route("/review/add", name="add_review", methods="POST")
    */
    public function addReview(Request $request) : response
    {
        $reviewContent = $request->request->all()['content'];

        if($this->getUser()) {
            $user = $this->getUser();
            $entityManager = $this->getDoctrine()->getManager();
            $review = new Review();
            $review->setUser($user);
            $review->setContent($reviewContent);
            $review->setSeller('emma');
            $review->setCreatedAt(new DateTimeImmutable('NOW'));
            $entityManager->persist($review);
            $entityManager->flush();
            $review = "hey";
        }else{
            return $this->redirect("http://localhost:8000/member");
        }

        return $this->json(['code' => 200, 'author' => $user->getUsername(), 'review' => $reviewContent, 200]);
    }

    /**
    * @Route("/review/get", name="get_review", methods="GET")
    */
    public function getReview(Request $request) : response
    {
        $seller = $request->query->get("seller");

        $repository = $this->getDoctrine()->getRepository(Review::class);
        $reviews = $repository->findBy(
            ['seller' => $seller]
        );

        return $this->json(['code' => 200, 'reviews' => $reviews, 200]);
    }
}
